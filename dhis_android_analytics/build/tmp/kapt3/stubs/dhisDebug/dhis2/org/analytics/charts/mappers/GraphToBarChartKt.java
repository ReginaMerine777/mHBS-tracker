package dhis2.org.analytics.charts.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"}, d2 = {"X_AXIS_MAX_PADDING_WITH_VALUE", "", "X_AXIS_MIN_PADDING", "dhis_android_analytics_dhisDebug"})
public final class GraphToBarChartKt {
    public static final float X_AXIS_MAX_PADDING_WITH_VALUE = 4.0F;
    public static final float X_AXIS_MIN_PADDING = -5.0F;
}